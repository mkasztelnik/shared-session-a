A::Application.config.session_store :redis_store,
                                    key: '_shared_session',
                                    expire_after: 40.minutes,
                                    domain: '.lvh.me',
                                    servers: [{
                                      host: "localhost",
                                      db: 4,
                                      namespace: "session",
                                      port: 6379,
                                      serializer: JSON
                                    }]
